准备工作：
0.1. 填写APIkey
0.2. 拷贝YeeLink目录到Debug目录

使用方法：
1. 刷新设备列表
2. 点击设备列表中的设备，会自动获取传感器列表，同时填写device id 到对应文本框（注意点击第2列会启动浏览器到目标设备）
3. 点击传感器列表中的传感器，会填写sensor id 到对应文本框,并发送读数据点命令（返回值显示在Response文本框中）
4. 点击APIs列表框中的API功能，会填写Httprequest相关内容（url, method， postData）
5. 点击执行按钮，就会执行对应的http操作

