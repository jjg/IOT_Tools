﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace  IOT_Tools
{



    class YeeLink
    {
        HTTPHelper helper;
        public YeeLink() {
            helper = new HTTPHelper();
        }
        public void Dispose(){
            helper = null;
        }


        public string HttpRequest(string method, string url, string apiKey, string postData)
        {   return (helper.YeeLink_Http(method, url, apiKey, postData));
        }
        //public string HttpRequest_Image(string method, string url, string apiKey, string postData)
        //{
        //    return (helper.YeeLink_Http_Image(method, url, apiKey, postData));
        //}
        //public string CreateDevice(string apiKey, string postData)
        //{
        //    return (helper.YeeLink_Http("POST","http://api.yeelink.net/v1.0/devices", apiKey, postData));
        //}

        //public string EditDevice(string devID, string apiKey, string postData)
        //{
        //    return (helper.YeeLink_Http("PUT","http://api.yeelink.net/v1.0/device/"+ devID, apiKey, postData));
        //}
        public string ListDevice(string apiKey)
        {
            return (helper.YeeLink_Http("GET", "http://api.yeelink.net/v1.0/devices", apiKey, ""));
        }

        public string ListSensors(string apiKey ,string devID)
        {
            return (helper.YeeLink_Http("GET", "http://api.yeelink.net/v1.0/device/"+ devID + "/sensors", apiKey, ""));
        }
        public string ListDataPoints(string apiKey, string devID, string sensorID)
        {
            return (helper.YeeLink_Http("GET", "http://api.yeelink.net/v1.0/device/"+devID+ "/sensor/"+ sensorID +"/datapoint/", apiKey, ""));
        }


        public YeeLinkParam GetYeeLinkParam(string s){
            YeeLinkParam ret = new YeeLinkParam();
            switch (s)
            {
                case "创建设备":
                    ret.url = "http://api.yeelink.net/v1.0/devices";
                    ret.httpBody = File.ReadAllText(Application.StartupPath + "\\YeeLink\\01.createDevice.txt");
                    ret.method  = "POST";
                    break;
                case "编辑设备":

                    ret.url = "http://api.yeelink.net/v1.0/device/<device_id>";
                    ret.httpBody = File.ReadAllText(Application.StartupPath + "\\YeeLink\\02.EditDevice.txt");
                    ret.method = "PUT";
                    break;
                case "罗列设备":
                    ret.url = "http://api.yeelink.net/v1.0/devices";
                    ret.httpBody = null;
                    ret.method = "GET";
                    break;
                case "查看设备":
                    ret.url = "http://api.yeelink.net/v1.0/device/<device_id>";
                    ret.httpBody = null;
                    ret.method = "GET";
                    break;
                case "删除设备":
                    ret.url = "http://api.yeelink.net/v1.0/device/<device_id>";
                    ret.httpBody = null;
                    ret.method = "DELETE";
                    break;
                case "创建传感器":
                    ret.url = "http://api.yeelink.net/v1.0/device/<device_id>/sensors";
                    ret.httpBody = File.ReadAllText(Application.StartupPath + "\\YeeLink\\03.createSensor.txt");
                    ret.method = "POST";
                    break;
                case "编辑传感器":
                    ret.url = "http://api.yeelink.net/v1.0/device/<device_id>/sensor/<sensor_id>";
                    ret.httpBody = File.ReadAllText(Application.StartupPath + "\\YeeLink\\04.EditSensor.txt");
                    ret.method = "PUT";
                    break;
                case "罗列传感器":
                    ret.url = "http://api.yeelink.net/v1.0/device/<device_id>/sensors";
                    ret.httpBody = null;
                    ret.method = "GET";
                    break;
                case "查看传感器":
                    ret.url = "http://api.yeelink.net/v1.0/device/<device_id>/sensor/<sensor_id>";
                    ret.httpBody = null;
                    ret.method = "GET";
                    break;
                case "删除传感器":
                    ret.url = "http://api.yeelink.net/v1.0/device/<device_id>/sensor/<sensor_id>";
                    ret.httpBody = null;
                    ret.method = "DELETE";
                    break;
                case "创建数据点":
                    ret.url = "http://api.yeelink.net/v1.0/device/<device_id>/sensor/<sensor_id>/datapoints";
                    ret.httpBody = File.ReadAllText(Application.StartupPath + "\\YeeLink\\05.createDataPoint.txt");
                    ret.method = "POST";
                    break;
                case "编辑数据点":
                    ret.url = "http://api.yeelink.net/v1.0/device/<device_id>/sensor/<sensor_id>/datapoint/<key>";
                    ret.httpBody = File.ReadAllText(Application.StartupPath + "\\YeeLink\\06.EditDataPoint.txt");
                    ret.method = "PUT";
                    break;
                case "查看数据点":
                    ret.url = "http://api.yeelink.net/v1.0/device/<device_id>/sensor/<sensor_id>/datapoint/<key>";
                    ret.httpBody = null;
                    ret.method = "GET";
                    break;
                case "删除数据点":
                    ret.url = "http://api.yeelink.net/v1.0/device/<device_id>/sensor/<sensor_id>/datapoint/<key>";
                    ret.httpBody = null;
                    ret.method = "DELETE";
                    break;
                case "上传图像":
                    ret.url = "http://api.yeelink.net/v1.0/device/<device_id>/sensor/<sensor_id>/photos";
                    ret.image =  File.ReadAllBytes(Application.StartupPath + "\\YeeLink\\宇宙02K.jpg");
                    string str = "{\"value\":";
                    str+= Comm.ToHexString(ret.image);
                    str += "}";
                    ret.httpBody = str;

                    ret.method = "POST";
                    break;
                case "获取图像信息":

                    break;
                case "获取图像内容":

                    break;

            }
            return ret;
        }
        //*************************************************************

    }
}
