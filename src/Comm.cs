﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace IOT_Tools
{
    public class Comm
    {
        static string fINI = Application.StartupPath + "\\app.ini";
        //*****************************************************************
        public static SysConfig LoadParam()   //加载程序配置
        {   INI ini = new INI(fINI);
            SysConfig cfg = new SysConfig();
            if (File.Exists(fINI))
            {   cfg.apiKey = ini.Read("APIKey");
                cfg.tab = ini.Read("TAB");
            }
            ini = null;
            return cfg;
        }
        public static void SaveParam(string apiKey,string tab)   //加载程序配置
        {   INI ini = new INI(fINI);
            if (apiKey != null) { ini.Write("APIKey", apiKey); }
            if (tab != null) { ini.Write("TAB", tab); }
            ini = null;
        }

        //*****************************************************************
        public static void PushGrid_Batch(List<YeeLinkDevice> lists, DataGridView dgv)
        {
            if (lists == null) return;
            dgv.Rows.Clear();
            DataGridViewRow row;
            int index = 0;
            for (int i = 0; i < lists.Count; i++)
            {
                index = dgv.Rows.Add();
                row = dgv.Rows[index];
                row.Cells[0].Value = index + 1;
                row.Cells[1].Value = lists[i].title;
                row.Cells[2].Value = lists[i].about;
                row.Cells[3].Value = lists[i].id;
                dgv.FirstDisplayedScrollingRowIndex = dgv.Rows[index].Index;
            }
        }
        public static void PushGrid_Batch(List<YeeLinkSensor> lists, DataGridView dgv)
        {
            if (lists == null) return;
            dgv.Rows.Clear();
            DataGridViewRow row;
            int index = 0;
            for (int i = 0; i < lists.Count; i++)
            {
                index = dgv.Rows.Add();
                row = dgv.Rows[index];
                row.Cells[0].Value = index + 1;
                row.Cells[1].Value = lists[i].title;
                row.Cells[2].Value = lists[i].about;
                row.Cells[3].Value = lists[i].id;
                dgv.FirstDisplayedScrollingRowIndex = dgv.Rows[index].Index;
            }
        }
        //*****************************************************************
        public static void DataGridViewInit(DataGridView dgv, bool isSensor = false)
        {   dgv.ReadOnly = true;
            dgv.DataSource = null;
            dgv.Columns.Clear();
            dgv.Rows.Clear();
            dgv.ShowCellToolTips = false;
            dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            DIC_YeeLink_Display.Load_GridHeader(dgv, isSensor);
            dgv.Columns[0].Width = 40;
            dgv.Columns[1].DefaultCellStyle.ForeColor = System.Drawing.Color.Blue;
            dgv.MultiSelect = false;
        }
        //*****************************************************************
        public void TestProperty()
        {

            YeeLinkDevice dev = new YeeLinkDevice();
            string str = typeof(YeeLinkDevice).GetProperties().First().Name;

            foreach (var item in dev.GetType().GetProperties())    //返回Student的所有公共属性
            {
                Debug.Print(item.Name);
                //var value = item.GetValue(dev, null);   //返回属性值    
                //var setobj = dev.GetType().GetProperty(item.Name);   //搜索具有指定属性名称的公共属性
                //if (value != null && setobj != null)
                //{
                //    setobj.SetValue(dev, value, null);
                //}
            }
        }

        public static string ToHexString(byte[] bytes) // 0xae00cf => "AE00CF "
        {
            string hexString = string.Empty;
            if (bytes != null)
            {   StringBuilder strB = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {   strB.Append(bytes[i].ToString("X2"));
                }
                hexString = strB.ToString();
            }
            return hexString;
        }
        //*****************************************************************
    }
}

