﻿using IOT_Tools.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IOT_Tools
{

    public class DIC_YeeLink_Display
    {   private static ArrayList dic = new ArrayList();
        private static string GetDic(int id)
        {   return dic[id].ToString();
        }
        public static void Load_GridHeader(DataGridView dgv,bool isSensor) {
            dic.Clear();
            dic.Add("编号");
            dic.Add("名称");
            dic.Add("描述");
            if (isSensor)
            {   dic.Add("sensorID");
            }
            else {
                dic.Add("deviceID");
            }
            
            for (int i = 0; i < dic.Count; i++)
            {   int index = dgv.Columns.Add(new DataGridViewTextBoxColumn());
                dgv.Columns[i].HeaderText = GetDic(i);
            }
        }
    }

    public class DIC_YeeLink_Method
    {
        private static ArrayList dic = new ArrayList();
        private static string GetDic(int id)
        {    return dic[id].ToString();
        }
        public static void Load_ListBox(ListBox lst)
        {   dic.Clear();
            dic.Add("创建设备");
            dic.Add("编辑设备");
            dic.Add("罗列设备");
            dic.Add("查看设备");
            dic.Add("删除设备");
            dic.Add("创建传感器");
            dic.Add("编辑传感器");
            dic.Add("罗列传感器");
            dic.Add("查看传感器");
            dic.Add("删除传感器");
            dic.Add("创建数据点");
            //dic.Add("编辑数据点");//测试失败
            dic.Add("查看数据点");
            //dic.Add("删除数据点");//测试失败
            //dic.Add("上传图像");
            //dic.Add("获取图像信息");
            //dic.Add("获取图像内容");
            for (int i = 0; i < dic.Count; i++)
            {   lst.Items.Add( GetDic(i));
            }
        }
}

    public class YeeLinkParam
    {   public string method;
        public string url;
        public string httpBody;
        public byte[] image;
    }
    public class YeeLinkDevice
    {   public string title { get; set; }   //名称
        public string about { get; set; }   //描述
        public string id { get; set; }      //deviceID
        public override string ToString()
        {   return title + "\t" + about + "\t" + id;
        }
    }
    public class YeeLinkSensor
    {   public string title { get; set; }   //名称
        public string about { get; set; }   //描述
        public string id { get; set; }      //deviceID
        public override string ToString()
        {
            return title + "\t" + about + "\t" + id;
        }
    }
    public class YeeLinkDataPoint
    {
        public string timestamp { get; set; }
        public string value { get; set; }
        public override string ToString()
        {
            return timestamp + "\t" + value + "\t" ;
        }
    }

    public class SysConfig
    {   public string apiKey { get; set; }
        public string tab { get; set; }
    }


    public class Student
    {   public int ID { get; set; }
        public string Name { get; set; }
        public string NickName { get; set; }
    }







}
