﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IOT_Tools
{
    public partial class frmYeeLink : Form
    {   YeeLink yeeLink;
        byte[] mbyt;
        public frmYeeLink()
        {
            
            InitializeComponent();
            yeeLink = new YeeLink();

            SysConfig cfg = Comm.LoadParam();
            if (cfg.apiKey != null)
            {
                txtAPIKey.Text = cfg.apiKey;
            }
            Comm.DataGridViewInit(dataGridView1, false);
            Comm.DataGridViewInit(dataGridView2,true);
            DIC_YeeLink_Method.Load_ListBox(lstAPIs);
        }
        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            yeeLink = null;
        }

        //*****************************************************************
        private void btnDo_Click(object sender, EventArgs e)
        {
            //txtResponse.Text = yeeLink.CreateDevice( apiKey, txtBody.Text );
            //txtResponse.Text = yeeLink.EditDevice("353074", apiKey, txtBody.Text);

            txtResponse.Text = yeeLink.HttpRequest(txtMethod.Text, txtURL.Text, txtAPIKey.Text, txtBody.Text);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            if (txtAPIKey.Text == null) return;
            txtResponse.Text = yeeLink.ListDevice(txtAPIKey.Text);
            if (txtResponse.Text.Contains("错误")) return;
            List<YeeLinkDevice> devs = JsonHelper.DeserializeJsonToList<YeeLinkDevice>(txtResponse.Text);
            Comm.PushGrid_Batch(devs, dataGridView1);
        }
        private void lstAPIs_Click(object sender, EventArgs e)
        {
            txtBody.Text = "";
            txtURL.Text = "";
            txtMethod.Text = "";
            txtResponse.Text = "";
            if (lstAPIs.SelectedItem == null) return;

            string s = lstAPIs.SelectedItem.ToString();
            YeeLinkParam param = yeeLink.GetYeeLinkParam(s);
            if (param != null)
            {	if(param.method != null)txtMethod.Text = param.method;
                if (param.httpBody != null) txtBody.Text = param.httpBody;
                if (param.url != null)txtURL.Text = param.url;
                if (param.image != null) mbyt = param.image;
                UpdateText();

            }
        }
        private void UpdateText() {
            if (txtDevID.Text != "") txtURL.Text = txtURL.Text.Replace("<device_id>", txtDevID.Text);
            if (txtSensorID.Text != "") txtURL.Text = txtURL.Text.Replace("<sensor_id>", txtSensorID.Text);
            txtURL.Text = txtURL.Text.Replace("<key>", "");  //txtKey.Text


        }

        private void GetRemoteSendors() {
            if (txtAPIKey.Text == "") return;
            if (txtDevID.Text == "") return;
            txtResponse.Text = yeeLink.ListSensors(txtAPIKey.Text,txtDevID.Text);
            if (txtResponse.Text.Contains("错误")) return;
            List<YeeLinkSensor> sensors = JsonHelper.DeserializeJsonToList<YeeLinkSensor>(txtResponse.Text);
            //Comm.PushGrid_Batch(sensors, dataGridView1);
            Comm.PushGrid_Batch(sensors, dataGridView2);
        }
        private void GetRemoteDataPoints()
        {   if (txtAPIKey.Text == "") return;
            if (txtDevID.Text == "") return;
            txtResponse.Text = yeeLink.ListDataPoints(txtAPIKey.Text, txtDevID.Text, txtSensorID.Text );
            //if (txtResponse.Text.Contains("错误")) return;
            //List<YeeLinkDataPoint> sensors = JsonHelper.DeserializeJsonToList<YeeLinkDataPoint>(txtResponse.Text);
            //Comm.PushGrid_Batch(sensors, dataGridView1);
            //Comm.PushGrid_Batch(sensors, dataGridView2);
        }
        private void lstSensors_SelectedIndexChanged(object sender, EventArgs e) {}
        private void lstPoints_SelectedIndexChanged(object sender, EventArgs e) {}
        private void lstSensors_Click(object sender, EventArgs e)
        {   txtSensorID.Text = "";
            
        }

        private void lstPoints_Click(object sender, EventArgs e)
        {   
        }
        //*****************************************************************



        //*****************************************************************
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e){ }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1 && e.RowIndex != -1)
            {   string url = "http://developer.yeelink.net/zh_CN/device/manage?device_id=" + dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
                System.Diagnostics.Process.Start(url);//点击浏览设备
            }
            try
            {
                txtDevID.Text = "";
                txtSensorID.Text = "";
                txtKey.Text = "";
                if (dataGridView1.SelectedRows[0].Cells[3].Value != null)
                {
                    txtDevID.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
                    GetRemoteSendors();
                    UpdateText();
                }
                else
                {
                    txtDevID.Text = "null";
                }
            }
            catch { }
        }
        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e) { }
        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {   try
            {
                txtSensorID.Text = "";
                txtKey.Text = "";
                if (dataGridView1.SelectedRows[0].Cells[3].Value != null)
                {
                    txtSensorID.Text = dataGridView2.SelectedRows[0].Cells[3].Value.ToString();
                    GetRemoteDataPoints();
                    UpdateText();
                }
                else
                {
                    txtDevID.Text = "null";
                }
            }
            catch { }
        }
        //*****************************************************************
        private void txtAPIKey_TextChanged(object sender, EventArgs e)
        {   Comm.SaveParam(txtAPIKey.Text, tab1.TabIndex.ToString());
        }
        private void tab1_SelectedIndexChanged(object sender, EventArgs e)
        {   Comm.SaveParam(txtAPIKey.Text, tab1.SelectedIndex.ToString());
        }
    }
}
